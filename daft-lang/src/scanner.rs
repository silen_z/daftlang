use std::iter::Peekable;

#[derive(PartialEq, Debug, Clone)]
pub struct Token {
    pub kind: TokenKind,
    pub value: Option<String>,
    pub line: usize,
    pub col: usize,
}

impl Token {
    pub fn kind(&self) -> &TokenKind {
        &self.kind
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum TokenKind {
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    Comma,
    Dot,
    Minus,
    Plus,
    Semicolon,
    Star,
    Bang,
    BangEqual,
    Equal,
    EqualEqual,
    Less,
    LessEqual,
    Greater,
    GreaterEqual,
    Slash,
    StringLiteral,
    NumberLiteral,
    Identifier,
    And,
    Class,
    Else,
    False,
    For,
    Fun,
    If,
    Nil,
    Or,
    Print,
    Return,
    Super,
    This,
    True,
    Let,
    While,
    EOF,
}

pub struct Tokenizer<I: Iterator<Item = char>> {
    source: Peekable<I>,
    line: usize,
    col: usize,
}

impl<S: Iterator<Item = char>> Iterator for Tokenizer<S> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(c) = self.consume() {
            match c {
                '(' => Some(self.produce_token(TokenKind::LeftParen)),
                ')' => Some(self.produce_token(TokenKind::RightParen)),
                '{' => Some(self.produce_token(TokenKind::LeftBrace)),
                '}' => Some(self.produce_token(TokenKind::RightBrace)),
                ',' => Some(self.produce_token(TokenKind::Comma)),
                '.' => Some(self.produce_token(TokenKind::Dot)),
                '-' => Some(self.produce_token(TokenKind::Minus)),
                '+' => Some(self.produce_token(TokenKind::Plus)),
                ';' => Some(self.produce_token(TokenKind::Semicolon)),
                '*' => Some(self.produce_token(TokenKind::Star)),
                '!' => {
                    if let Some('=') = self.source.peek() {
                        let token = self.produce_token(TokenKind::BangEqual);
                        self.consume();
                        Some(token)
                    } else {
                        Some(self.produce_token(TokenKind::Bang))
                    }
                }
                '=' => {
                    if let Some('=') = self.source.peek() {
                        let token = self.produce_token(TokenKind::EqualEqual);
                        self.consume();
                        Some(token)
                    } else {
                        Some(self.produce_token(TokenKind::Equal))
                    }
                }
                '<' => {
                    if let Some('=') = self.source.peek() {
                        let token = self.produce_token(TokenKind::LessEqual);
                        self.consume();
                        Some(token)
                    } else {
                        Some(self.produce_token(TokenKind::Less))
                    }
                }
                '>' => {
                    if let Some('=') = self.source.peek() {
                        let token = self.produce_token(TokenKind::GreaterEqual);
                        self.consume();
                        Some(token)
                    } else {
                        Some(self.produce_token(TokenKind::Greater))
                    }
                }
                '/' => {
                    if let Some('/') = self.source.peek() {
                        while self.source.next() != Some('\n') {}
                        self.next()
                    } else {
                        Some(self.produce_token(TokenKind::Slash))
                    }
                }
                '"' => {
                    let mut string = String::new();
                    loop {
                        match self.consume() {
                            Some('\n') => {
                                self.col = 1;
                                self.line += 1;
                            }
                            Some('"') => {
                                break;
                            }
                            Some(c) => {
                                string.push(c);
                            }
                            None => panic!("unterminated string at {}:{}", self.line, self.col),
                        }
                    }

                    Some(self.produce_token_with_value(TokenKind::StringLiteral, string))
                }
                '\n' => {
                    self.col = 0;
                    self.line += 1;
                    self.next()
                }
                c if c.is_ascii_digit() => {
                    let mut literal = c.to_string();

                    while let Some(&c) = self.source.peek() {
                        if c.is_ascii_digit() || c == '.' {
                            self.consume();
                            literal.push(c);
                        } else {
                            break;
                        }
                    }

                    Some(self.produce_token_with_value(TokenKind::NumberLiteral, literal))
                }

                c if c.is_ascii_alphabetic() => {
                    let mut ident = c.to_string();
                    while let Some(c) = self
                        .source
                        .peek()
                        .filter(|c| c.is_ascii_alphanumeric() || **c == '_')
                        .copied()
                    {
                        self.consume();
                        ident.push(c);
                    }

                    if let Ok(keyword) = ident.parse() {
                        return Some(self.produce_token(keyword));
                    }

                    Some(self.produce_token_with_value(TokenKind::Identifier, ident.clone()))
                }
                c if c.is_ascii_whitespace() => self.next(),
                _ => panic!("unexpected character at {}:{}", self.line, self.col),
            }
        } else {
            Some(self.produce_token(TokenKind::EOF))
        }
    }
}

impl std::str::FromStr for TokenKind {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "and" => Ok(TokenKind::And),
            "class" => Ok(TokenKind::Class),
            "else" => Ok(TokenKind::Else),
            "false" => Ok(TokenKind::False),
            "for" => Ok(TokenKind::For),
            "fun" => Ok(TokenKind::Fun),
            "if" => Ok(TokenKind::If),
            "nil" => Ok(TokenKind::Nil),
            "or" => Ok(TokenKind::Or),
            "print" => Ok(TokenKind::Print),
            "return" => Ok(TokenKind::Return),
            "super" => Ok(TokenKind::Super),
            "this" => Ok(TokenKind::This),
            "true" => Ok(TokenKind::True),
            "let" => Ok(TokenKind::Let),
            "while" => Ok(TokenKind::While),
            _ => Err(()),
        }
    }
}

impl<S: Iterator<Item = char>> Tokenizer<S> {
    fn consume(&mut self) -> Option<char> {
        self.col += 1;
        self.source.next()
    }

    fn produce_token(&self, kind: TokenKind) -> Token {
        Token {
            kind,
            value: None,
            line: self.line,
            col: self.col,
        }
    }
    fn produce_token_with_value(&self, kind: TokenKind, value: String) -> Token {
        Token {
            kind,
            value: Some(value),
            line: self.line,
            col: self.col,
        }
    }
}

pub trait Tokenize {
    fn tokens(self) -> Tokenizer<Self>
    where
        Self: Iterator<Item = char> + Sized;
}

impl<I> Tokenize for I
where
    I: Iterator<Item = char>,
{
    fn tokens(self) -> Tokenizer<Self> {
        Tokenizer {
            source: self.peekable(),
            line: 1,
            col: 0,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Tokenize;
    use super::*;

    #[test]
    fn empty() {
        assert_eq!(
            "".chars().tokens().map(|t| t.kind).collect::<Vec<_>>(),
            vec![TokenKind::EOF]
        );
    }

    #[test]
    fn simple_tokens() {
        assert_eq!(
            "{*}".chars().tokens().map(|t| t.kind).collect::<Vec<_>>(),
            vec![
                TokenKind::LeftBrace,
                TokenKind::Star,
                TokenKind::RightBrace,
                TokenKind::EOF
            ]
        );
    }

    #[test]
    fn two_char_tokens() {
        assert_eq!(
            "=!=".chars().tokens().map(|t| t.kind).collect::<Vec<_>>(),
            vec![TokenKind::Equal, TokenKind::BangEqual, TokenKind::EOF]
        );
    }

    #[test]
    fn comments_and_whitespace() {
        assert_eq!(
            "{ // ignored\n  *\n}"
                .chars()
                .tokens()
                .map(|t| t.kind)
                .collect::<Vec<_>>(),
            vec![
                TokenKind::LeftBrace,
                TokenKind::Star,
                TokenKind::RightBrace,
                TokenKind::EOF
            ]
        );
    }

    #[test]
    fn string_literals() {
        assert_eq!(
            r#"{ "test" }"#.chars().tokens().map(|t| t.kind).collect::<Vec<_>>(),
            vec![
                TokenKind::LeftBrace,
                TokenKind::StringLiteral,
                TokenKind::RightBrace,
                TokenKind::EOF
            ]
        );
    }

    #[test]
    fn number_literals() {
        assert_eq!(
            "{ 1 + 1.1 }"
                .chars()
                .tokens()
                .map(|t| t.kind)
                .collect::<Vec<_>>(),
            vec![
                TokenKind::LeftBrace,
                TokenKind::NumberLiteral,
                TokenKind::Plus,
                TokenKind::NumberLiteral,
                TokenKind::RightBrace,
                TokenKind::EOF
            ]
        );
    }
}
