use crate::ast::*;
use crate::scanner::{Token, TokenKind};
use std::iter::Peekable;

#[derive(PartialEq, Debug)]
pub enum ParseError {
    UnexpectedToken { at: Token },
    ExpectedToken { expected: TokenKind, at: Token },
    InvalidLiteral(Token),
}

pub struct Parser<I: Iterator<Item = Token>> {
    iter: Peekable<I>,
}

impl<I: Iterator<Item = Token>> Iterator for Parser<I> {
    type Item = Result<Statement, ParseError>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(TokenKind::EOF) = self.peek_kind() {
            return None;
        }
        Some(self.parse_declaration())
    }
}

impl<I: Iterator<Item = Token>> Parser<I> {
    fn parse_declaration(&mut self) -> Result<Statement, ParseError> {
        if self.match_kind(TokenKind::Fun) {
            return self.parse_function_declaration();
        }

        if self.match_kind(TokenKind::Let) {
            return self.parse_variable_declaration();
        }

        self.parse_statement()
    }

    fn parse_function_declaration(&mut self) -> Result<Statement, ParseError> {
        let name = self.expect_identifier()?;

        self.expect_kind(TokenKind::LeftParen)?;

        let mut parameters = Vec::new();

        if self.peek_kind() != Some(&TokenKind::RightParen) {
            loop {
                let parameter = self.expect_identifier()?;
                parameters.push(parameter);

                if self.peek_kind() != Some(&TokenKind::Comma) {
                    break;
                } else {
                    self.iter.next();
                }
            }
        }

        self.expect_kind(TokenKind::RightParen)?;
        self.expect_kind(TokenKind::LeftBrace)?;

        let body = self.parse_block()?;

        Ok(Statement::FunctionDeclaration(name, parameters, body))
    }

    fn parse_block(&mut self) -> Result<Vec<Statement>, ParseError> {
        let mut statements = Vec::new();

        while self.peek_kind() != Some(&TokenKind::RightBrace) {
            let smts = self.parse_declaration()?;
            statements.push(smts);
        }

        self.expect_kind(TokenKind::RightBrace)?;

        Ok(statements)
    }

    fn parse_variable_declaration(&mut self) -> Result<Statement, ParseError> {
        let ident = self.expect_identifier()?;

        let mut initializer = None;

        if self.match_kind(TokenKind::Equal) {
            let expr = self.parse_expression()?;
            initializer = Some(expr);
        }

        self.expect_kind(TokenKind::Semicolon)?;

        Ok(Statement::VariableDeclaration(ident, initializer))
    }

    fn parse_expression(&mut self) -> Result<Expression, ParseError> {
        self.parse_assignment()
    }

    fn parse_assignment(&mut self) -> Result<Expression, ParseError> {
        let expr = self.parse_or()?;

        if self.match_kind(TokenKind::Equal) {
            let value = self.parse_assignment()?;

            if let Expression::Variable(ident) = expr {
                return Ok(Expression::Assignment(ident.clone(), Box::new(value)));
            }

            return Err(ParseError::ExpectedToken {
                at: self.iter.next().unwrap(),
                expected: TokenKind::Identifier,
            });
        }
        Ok(expr)
    }

    fn parse_or(&mut self) -> Result<Expression, ParseError> {
        let mut expr = self.parse_and()?;

        while self.match_kind(TokenKind::Or) {
            let right = self.parse_comparison()?;
            expr = Expression::Logical(Box::new(expr), LogicalOperator::Or, Box::new(right));
        }

        Ok(expr)
    }

    fn parse_and(&mut self) -> Result<Expression, ParseError> {
        let mut expr = self.parse_equality()?;

        while self.match_kind(TokenKind::And) {
            let right = self.parse_comparison()?;
            expr = Expression::Logical(Box::new(expr), LogicalOperator::And, Box::new(right));
        }

        Ok(expr)
    }

    fn parse_equality(&mut self) -> Result<Expression, ParseError> {
        let mut expr = self.parse_comparison()?;

        while matches!(
            self.peek_kind(),
            Some(TokenKind::BangEqual) | Some(TokenKind::EqualEqual)
        ) {
            let operator = self.iter.next().unwrap().kind().into();
            let right = self.parse_comparison()?;
            expr = Expression::Binary(Box::new(expr), operator, Box::new(right));
        }

        Ok(expr)
    }

    fn parse_comparison(&mut self) -> Result<Expression, ParseError> {
        let mut expr = self.parse_addition()?;

        while matches!(
            self.peek_kind(),
            Some(TokenKind::Greater)
                | Some(TokenKind::GreaterEqual)
                | Some(TokenKind::Less)
                | Some(TokenKind::LessEqual)
        ) {
            let operator = self.iter.next().unwrap().kind().into();
            let right = self.parse_addition()?;
            expr = Expression::Binary(Box::new(expr), operator, Box::new(right));
        }

        Ok(expr)
    }

    fn parse_addition(&mut self) -> Result<Expression, ParseError> {
        let mut expr = self.parse_multiplication()?;

        while matches!(
            self.peek_kind(),
            Some(TokenKind::Minus) | Some(TokenKind::Plus)
        ) {
            let operator = self.iter.next().unwrap().kind().into();
            let right = self.parse_multiplication()?;
            expr = Expression::Binary(Box::new(expr), operator, Box::new(right));
        }

        Ok(expr)
    }

    fn parse_multiplication(&mut self) -> Result<Expression, ParseError> {
        let mut expr = self.parse_unary()?;

        while matches!(
            self.peek_kind(),
            Some(TokenKind::Star) | Some(TokenKind::Slash)
        ) {
            let operator = self.iter.next().unwrap().kind().into();
            let right = self.parse_unary()?;
            expr = Expression::Binary(Box::new(expr), operator, Box::new(right));
        }

        Ok(expr)
    }

    fn parse_unary(&mut self) -> Result<Expression, ParseError> {
        if matches!(
            self.peek_kind(),
            Some(TokenKind::Bang) | Some(TokenKind::Minus)
        ) {
            let operator = self.iter.next().unwrap().kind().into();
            let right = self.parse_unary()?;
            return Ok(Expression::Unary(operator, Box::new(right)));
        }

        self.parse_function_call_open()
    }

    fn parse_function_call_open(&mut self) -> Result<Expression, ParseError> {
        let mut expr = self.parse_primary()?;

        loop {
            if self.match_kind(TokenKind::LeftParen) {
                expr = self.parse_function_call_close(expr)?;
            } else {
                break;
            }
        }

        Ok(expr)
    }

    fn parse_primary(&mut self) -> Result<Expression, ParseError> {
        if self.match_kind(TokenKind::False) {
            return Ok(Expression::Literal(Literal::False));
        }

        if self.match_kind(TokenKind::True) {
            return Ok(Expression::Literal(Literal::True));
        }

        if self.match_kind(TokenKind::Nil) {
            return Ok(Expression::Literal(Literal::Nil));
        }

        if let Some(Token {
            kind: TokenKind::NumberLiteral,
            value: Some(number),
            ..
        }) = self.iter.peek()
        {
            let number = number
                .parse()
                .map_err(|_| ParseError::InvalidLiteral(self.iter.peek().cloned().unwrap()))?;
            self.iter.next();
            let literal = Literal::Number(number);
            return Ok(Expression::Literal(literal));
        }

        if let Some(Token {
            kind: TokenKind::StringLiteral,
            value: Some(string),
            ..
        }) = self.iter.peek()
        {
            let literal = Literal::String(string.clone());
            self.iter.next();
            return Ok(Expression::Literal(literal));
        }

        if let Some(Token {
            kind: TokenKind::Identifier,
            value: Some(ident),
            ..
        }) = self.iter.peek()
        {
            let ident = ident.clone();
            self.iter.next();
            return Ok(Expression::Variable(ident));
        }

        if self.match_kind(TokenKind::LeftParen) {
            let expr = self.parse_expression()?;

            self.expect_kind(TokenKind::RightParen)?;
            return Ok(Expression::Grouping(Box::new(expr)));
        }

        Err(ParseError::UnexpectedToken {
            at: self.iter.next().unwrap(),
        })
    }

    fn parse_function_call_close(&mut self, callee: Expression) -> Result<Expression, ParseError> {
        let mut arguments = Vec::new();
        if self.peek_kind() != Some(&TokenKind::RightParen) {
            loop {
                // TODO: improve this loop?
                let argument = self.parse_expression()?;
                arguments.push(argument);
                if let Some(TokenKind::Comma) = self.peek_kind() {
                    self.iter.next();
                } else {
                    break;
                }
            }
        }
        self.expect_kind(TokenKind::RightParen)?;

        return Ok(Expression::Call(Box::new(callee), arguments));
    }

    fn parse_statement(&mut self) -> Result<Statement, ParseError> {
        if self.match_kind(TokenKind::If) {
            return self.parse_if_statement();
        }

        if self.match_kind(TokenKind::Print) {
            return self.parse_print_statement();
        }

        if self.match_kind(TokenKind::Return) {
            return self.parse_return_statement();
        }

        if self.match_kind(TokenKind::While) {
            return self.parse_while_statement();
        }

        if self.match_kind(TokenKind::LeftBrace) {
            let stmts = self.parse_block()?;
            return Ok(Statement::Block(stmts));
        }

        let expr = self.parse_expression()?;

        self.expect_kind(TokenKind::Semicolon)?;

        Ok(Statement::ExprStatement(expr))
    }

    fn parse_if_statement(&mut self) -> Result<Statement, ParseError> {
        self.expect_kind(TokenKind::LeftParen)?;

        let condition = self.parse_expression()?;

        self.expect_kind(TokenKind::RightParen)?;

        let then_branch = self.parse_statement()?;

        let else_branch = if self.match_kind(TokenKind::Else) {
            let stmt = self.parse_statement()?;
            Some(Box::new(stmt))
        } else {
            None
        };

        Ok(Statement::If(condition, Box::new(then_branch), else_branch))
    }

    fn parse_print_statement(&mut self) -> Result<Statement, ParseError> {
        let expr = self.parse_expression()?;

        self.expect_kind(TokenKind::Semicolon)?;

        Ok(Statement::Print(expr))
    }

    fn parse_return_statement(&mut self) -> Result<Statement, ParseError> {
        if self.match_kind(TokenKind::Semicolon) {
            return Ok(Statement::Return(None));
        }

        let expr = self.parse_expression()?;

        self.expect_kind(TokenKind::Semicolon)?;

        Ok(Statement::Return(Some(expr)))
    }

    fn parse_while_statement(&mut self) -> Result<Statement, ParseError> {
        self.expect_kind(TokenKind::LeftParen)?;

        let condition = self.parse_expression()?;

        self.expect_kind(TokenKind::RightParen)?;

        let body = self.parse_statement()?;

        Ok(Statement::While(condition, Box::new(body)))
    }

    fn peek_kind(&mut self) -> Option<&TokenKind> {
        self.iter.peek().map(|t| t.kind())
    }

    fn match_kind(&mut self, kind: TokenKind) -> bool {
        if self.peek_kind() == Some(&kind) {
            self.iter.next();
            true
        } else {
            false
        }
    }

    fn expect_kind(&mut self, expected: TokenKind) -> Result<(), ParseError> {
        if self.peek_kind() == Some(&expected) {
            self.iter.next();
            Ok(())
        } else {
            Err(ParseError::ExpectedToken {
                at: self.iter.next().unwrap(),
                expected,
            })
        }
    }

    fn expect_identifier(&mut self) -> Result<String, ParseError> {
        if let Some(Token {
            kind: TokenKind::Identifier,
            value: Some(ident),
            ..
        }) = self.iter.peek()
        {
            let ident = ident.clone();
            self.iter.next();
            return Ok(ident);
        }

        Err(ParseError::ExpectedToken {
            at: self.iter.next().unwrap(),
            expected: TokenKind::Identifier,
        })
    }
}

pub trait Parse {
    fn parse(self) -> Parser<Self>
    where
        Self: Iterator<Item = Token> + Sized;
}

impl<I: Iterator<Item = Token>> Parse for I {
    fn parse(self) -> Parser<Self> {
        Parser {
            iter: self.peekable(),
        }
    }
}
