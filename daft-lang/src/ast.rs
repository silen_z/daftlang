use crate::scanner::TokenKind;

#[derive(Debug, PartialEq, Clone)]
pub enum Statement {
    Print(Expression),
    ExprStatement(Expression),
    VariableDeclaration(String, Option<Expression>),
    FunctionDeclaration(String, Vec<String>, Vec<Statement>),
    Block(Vec<Statement>),
    If(Expression, Box<Statement>, Option<Box<Statement>>),
    While(Expression, Box<Statement>),
    Return(Option<Expression>),
}

#[derive(Debug, PartialEq, Clone)]
pub enum Expression {
    Literal(Literal),
    Unary(UnaryOperator, Box<Expression>),
    Binary(Box<Expression>, Operator, Box<Expression>),
    Grouping(Box<Expression>),
    Variable(String),
    Assignment(String, Box<Expression>),
    Logical(Box<Expression>, LogicalOperator, Box<Expression>),
    Call(Box<Expression>, Vec<Expression>),
}

#[derive(Debug, PartialEq, Clone)]
pub enum Literal {
    True,
    False,
    Nil,
    String(String),
    Number(f64),
}

#[derive(Debug, PartialEq, Clone)]
pub enum Operator {
    Equal,
    NotEqual,
    Less,
    LessOrEqual,
    Greater,
    GreaterOrEqual,
    Plus,
    Minus,
    Multiply,
    Divide,
}

impl From<&TokenKind> for Operator {
    fn from(token: &TokenKind) -> Self {
        match token {
            TokenKind::EqualEqual => Operator::Equal,
            TokenKind::BangEqual => Operator::NotEqual,
            TokenKind::Less => Operator::Less,
            TokenKind::LessEqual => Operator::LessOrEqual,
            TokenKind::Greater => Operator::Greater,
            TokenKind::GreaterEqual => Operator::GreaterOrEqual,
            TokenKind::Plus => Operator::Plus,
            TokenKind::Minus => Operator::Minus,
            TokenKind::Star => Operator::Multiply,
            TokenKind::Slash => Operator::Divide,
            _ => panic!("can't convert {:?} into Operator", token),
        }
    }
}

impl std::fmt::Display for Operator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Operator::Equal => write!(f, "=="),
            Operator::NotEqual => write!(f, "!="),
            Operator::Less => write!(f, "<"),
            Operator::LessOrEqual => write!(f, "<="),
            Operator::Greater => write!(f, ">"),
            Operator::GreaterOrEqual => write!(f, ">="),
            Operator::Plus => write!(f, "+"),
            Operator::Minus => write!(f, "-"),
            Operator::Multiply => write!(f, "*"),
            Operator::Divide => write!(f, "/"),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum LogicalOperator {
    And,
    Or,
}

#[derive(Debug, PartialEq, Clone)]
pub enum UnaryOperator {
    Not,
    Minus,
}

impl From<&TokenKind> for UnaryOperator {
    fn from(token: &TokenKind) -> Self {
        match token {
            TokenKind::Bang => UnaryOperator::Not,
            TokenKind::Minus => UnaryOperator::Minus,
            _ => panic!("can't convert {:?} into UnaryOperator", token),
        }
    }
}