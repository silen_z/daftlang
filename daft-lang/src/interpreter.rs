use crate::ast::*;
use std::collections::HashMap;
use std::{cell::RefCell, io::Write, rc::Rc};

pub struct Interpreter<O: Write> {
    env: Rc<Environment>,
    output: O,
}

impl<O: Write> Interpreter<O> {
    pub fn new(output: O) -> Self {
        Self {
            env: Rc::new(Environment::default()),
            output,
        }
    }

    pub fn interpret(&mut self, statement: &Statement) -> Result<(), RuntimeError> {
        self.execute(statement)?;
        Ok(())
    }

    fn execute(&mut self, statement: &Statement) -> Result<Option<RuntimeValue>, RuntimeError> {
        match statement {
            Statement::Print(expr) => {
                let value = self.evaulate(expr)?;
                writeln!(self.output, "{}", value)?;
                Ok(None)
            }
            Statement::ExprStatement(expr) => {
                self.evaulate(expr)?;
                Ok(None)
            }
            Statement::VariableDeclaration(ident, init) => {
                let mut value = RuntimeValue::Nil;
                if let Some(init) = init {
                    value = self.evaulate(init)?;
                }
                self.env.declare(ident.clone(), value);
                Ok(None)
            }
            Statement::Block(stmts) => {
                let env = Environment::child(&self.env);
                self.execute_block(stmts, env)
            }
            Statement::If(cond, then_branch, else_branch) => {
                let cond = self.evaulate(cond)?;
                if cond.is_truthy()? {
                    self.execute(then_branch)
                } else if let Some(else_branch) = else_branch {
                    self.execute(else_branch)
                } else {
                    Ok(None)
                }
            }
            Statement::While(cond, body) => {
                while self.evaulate(cond)?.is_truthy()? {
                    if let Some(return_value) = self.execute(body)? {
                        return Ok(Some(return_value));
                    }
                }
                Ok(None)
            }
            Statement::FunctionDeclaration(name, params, body) => {
                let closure = Environment::child(&self.env);

                let function = RuntimeValue::Function(
                    name.clone(),
                    Function::UserDefined(params.clone(), body.clone(), closure),
                );

                self.env.declare(name.clone(), function);
                Ok(None)
            }
            Statement::Return(expr) => {
                if let Some(expr) = expr {
                    let value = self.evaulate(expr)?;
                    return Ok(Some(value));
                }
                Ok(Some(RuntimeValue::Nil))
            }
        }
    }

    fn execute_block(
        &mut self,
        stmts: &[Statement],
        env: Rc<Environment>,
    ) -> Result<Option<RuntimeValue>, RuntimeError> {
        let previous = std::mem::replace(&mut self.env, env);

        for stmt in stmts {
            let return_value = self.execute(stmt)?;

            if return_value.is_some() {
                std::mem::replace(&mut self.env, previous);
                return Ok(return_value);
            }
        }

        std::mem::replace(&mut self.env, previous);
        Ok(None)
    }

    fn evaulate(&mut self, expr: &Expression) -> Result<RuntimeValue, RuntimeError> {
        match expr {
            Expression::Literal(literal) => Ok(RuntimeValue::from(literal)),
            Expression::Unary(op, expr) => {
                let val = self.evaulate(expr)?;
                match (op, val) {
                    (UnaryOperator::Not, RuntimeValue::Boolean(b)) => Ok(RuntimeValue::Boolean(!b)),
                    (UnaryOperator::Not, _) => Err(RuntimeError::TypeError(
                        "only booleans can be negated with !".to_owned(),
                    )),
                    (UnaryOperator::Minus, RuntimeValue::Number(n)) => Ok(RuntimeValue::Number(-n)),
                    (UnaryOperator::Minus, _) => Err(RuntimeError::TypeError(
                        "only numbers can be negated with -".to_owned(),
                    )),
                }
            }
            Expression::Binary(left, op, right) => {
                let left = self.evaulate(left)?;
                let right = self.evaulate(right)?;

                match (op, left, right) {
                    (Operator::Equal, l, r) => Ok(RuntimeValue::Boolean(l.eq(&r))),
                    (Operator::NotEqual, l, r) => Ok(RuntimeValue::Boolean(l.ne(&r))),
                    (Operator::Less, RuntimeValue::Number(l), RuntimeValue::Number(r)) => {
                        Ok(RuntimeValue::Boolean(l.lt(&r)))
                    }
                    (Operator::LessOrEqual, RuntimeValue::Number(l), RuntimeValue::Number(r)) => {
                        Ok(RuntimeValue::Boolean(l.le(&r)))
                    }
                    (Operator::Greater, RuntimeValue::Number(l), RuntimeValue::Number(r)) => {
                        Ok(RuntimeValue::Boolean(l.gt(&r)))
                    }
                    (
                        Operator::GreaterOrEqual,
                        RuntimeValue::Number(l),
                        RuntimeValue::Number(r),
                    ) => Ok(RuntimeValue::Boolean(l.ge(&r))),
                    (Operator::Less, _, _)
                    | (Operator::LessOrEqual, _, _)
                    | (Operator::Greater, _, _)
                    | (Operator::GreaterOrEqual, _, _) => Err(RuntimeError::TypeError(
                        "only numbers can be compared".to_owned(),
                    )),
                    (Operator::Plus, RuntimeValue::Number(l), RuntimeValue::Number(r)) => {
                        Ok(RuntimeValue::Number(l + r))
                    }
                    (Operator::Plus, RuntimeValue::String(l), RuntimeValue::String(r)) => {
                        Ok(RuntimeValue::String(l + &r))
                    }
                    (Operator::Plus, _, _) => Err(RuntimeError::TypeError(
                        "only numbers and string can be concatenated".to_owned(),
                    )),
                    (Operator::Minus, RuntimeValue::Number(l), RuntimeValue::Number(r)) => {
                        Ok(RuntimeValue::Number(l - r))
                    }
                    (Operator::Multiply, RuntimeValue::Number(l), RuntimeValue::Number(r)) => {
                        Ok(RuntimeValue::Number(l * r))
                    }
                    (Operator::Divide, RuntimeValue::Number(l), RuntimeValue::Number(r)) => {
                        Ok(RuntimeValue::Number(l / r))
                    }
                    (Operator::Minus, _, _)
                    | (Operator::Divide, _, _)
                    | (Operator::Multiply, _, _) => Err(RuntimeError::TypeError(
                        "artitetical operation only work on numbers".to_owned(),
                    )),
                }
            }
            Expression::Logical(left, LogicalOperator::And, right) => {
                let left = self.evaulate(left)?;

                if left.is_truthy()? {
                    let right = self.evaulate(right)?;
                    Ok(RuntimeValue::Boolean(right.is_truthy()?))
                } else {
                    Ok(RuntimeValue::Boolean(false))
                }
            }
            Expression::Logical(left, LogicalOperator::Or, right) => {
                let left = self.evaulate(left)?;

                if left.is_truthy()? {
                    Ok(RuntimeValue::Boolean(true))
                } else {
                    let right = self.evaulate(right)?;
                    Ok(RuntimeValue::Boolean(right.is_truthy()?))
                }
            }
            Expression::Variable(ident) => self.env.get(ident),
            Expression::Assignment(ident, expr) => {
                let value = self.evaulate(expr)?;
                self.env.assign(ident, value.clone())
            }
            Expression::Call(callee, args) => {
                if let RuntimeValue::Function(_, f) = self.evaulate(callee)? {
                    let args = args
                        .iter()
                        .map(|expr| self.evaulate(expr))
                        .collect::<Result<Vec<_>, _>>()?;

                    let return_value = self.call_function(f, &args)?;
                    Ok(return_value)
                } else {
                    Err(RuntimeError::TypeError(String::from(
                        "can't call non-function",
                    )))
                }
            }
            Expression::Grouping(expr) => self.evaulate(expr),
        }
    }

    fn call_function(
        &mut self,
        f: Function,
        args: &[RuntimeValue],
    ) -> Result<RuntimeValue, RuntimeError> {
        match f {
            Function::Native(f) => Ok(f(args)),
            Function::UserDefined(params, body, closure) => {
                let scope_env = closure.child();
                for (i, param) in params.iter().enumerate() {
                    let arg = args.get(i).ok_or(RuntimeError::MissingArgument)?;
                    scope_env.declare(param.clone(), arg.clone());
                }

                if let Some(returned) = self.execute_block(&body, scope_env)? {
                    return Ok(returned);
                }

                Ok(RuntimeValue::Nil)
            }
        }
    }
}

#[derive(Clone, Debug)]
pub struct Environment {
    parent: Option<Rc<Environment>>,
    values: RefCell<HashMap<String, RuntimeValue>>,
}

impl Environment {
    fn declare(&self, ident: String, value: RuntimeValue) {
        self.values.borrow_mut().insert(ident.clone(), value);
    }

    fn assign(&self, ident: &String, value: RuntimeValue) -> Result<RuntimeValue, RuntimeError> {
        if let Some(variable) = self.values.borrow_mut().get_mut(ident) {
            *variable = value.clone();
            return Ok(value);
        }

        if let Some(parent) = &self.parent {
            parent.assign(ident, value)
        } else {
            Err(RuntimeError::UndefinedVariable(ident.clone()))
        }
    }

    fn get(&self, ident: &String) -> Result<RuntimeValue, RuntimeError> {
        if let Some(value) = self.values.borrow().get(ident) {
            return Ok(value.clone());
        }

        if let Some(parent) = &self.parent {
            parent.get(ident)
        } else {
            Err(RuntimeError::UndefinedVariable(ident.clone()))
        }
    }

    fn child(self: &Rc<Environment>) -> Rc<Environment> {
        Rc::new(Environment {
            parent: Some(self.clone()),
            values: RefCell::new(HashMap::new()),
        })
    }
}

impl Default for Environment {
    fn default() -> Self {
        let mut globals = HashMap::new();

        globals.insert(
            String::from("clock"),
            RuntimeValue::Function(String::from("clock"), Function::Native(&clock)),
        );

        globals.insert(
            String::from("num"),
            RuntimeValue::Function(String::from("num"), Function::Native(&convert_to_number)),
        );

        globals.insert(
            String::from("str"),
            RuntimeValue::Function(String::from("str"), Function::Native(&convert_to_string)),
        );

        Self {
            values: RefCell::new(globals),
            parent: None,
        }
    }
}

#[derive(Debug)]
pub enum RuntimeError {
    TypeError(String),
    UndefinedVariable(String),
    MissingArgument,
    IoError(std::io::Error),
}

impl From<std::io::Error> for RuntimeError {
    fn from(error: std::io::Error) -> Self {
        RuntimeError::IoError(error)
    }
}

#[derive(Clone)]
pub enum RuntimeValue {
    String(String),
    Number(f64),
    Boolean(bool),
    Nil,
    Function(String, Function),
}

impl RuntimeValue {
    fn is_truthy(&self) -> Result<bool, RuntimeError> {
        match self {
            RuntimeValue::Boolean(b) => Ok(*b),
            RuntimeValue::Nil => Ok(false),
            _ => Err(RuntimeError::TypeError(String::from("expected boolean"))),
        }
    }
}

#[derive(Clone)]
pub enum Function {
    Native(&'static dyn Fn(&[RuntimeValue]) -> RuntimeValue),
    UserDefined(Vec<String>, Vec<Statement>, Rc<Environment>),
}

impl PartialEq for RuntimeValue {
    fn eq(&self, other: &RuntimeValue) -> bool {
        match (&self, other) {
            (RuntimeValue::String(s1), RuntimeValue::String(s2)) => s1.eq(s2),
            (RuntimeValue::Number(n1), RuntimeValue::Number(n2)) => n1.eq(n2),
            (RuntimeValue::Boolean(b1), RuntimeValue::Boolean(b2)) => b1.eq(b2),
            (RuntimeValue::Nil, RuntimeValue::Nil) => true,
            _ => false,
        }
    }
}

impl From<&Literal> for RuntimeValue {
    fn from(literal: &Literal) -> Self {
        match literal {
            Literal::Nil => RuntimeValue::Nil,
            Literal::True => RuntimeValue::Boolean(true),
            Literal::False => RuntimeValue::Boolean(false),
            Literal::Number(n) => RuntimeValue::Number(*n),
            Literal::String(s) => RuntimeValue::String(s.clone()),
        }
    }
}

impl std::fmt::Display for RuntimeValue {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            RuntimeValue::String(s) => write!(f, "{}", s),
            RuntimeValue::Number(n) => write!(f, "{}", n),
            RuntimeValue::Boolean(b) => write!(f, "{}", b),
            RuntimeValue::Nil => write!(f, "nil"),
            RuntimeValue::Function(name, Function::Native(_)) => write!(f, "<native fn {}>", name),
            RuntimeValue::Function(name, _) => write!(f, "<fn {}>", name),
        }
    }
}

impl std::fmt::Debug for RuntimeValue {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

fn clock(_: &[RuntimeValue]) -> RuntimeValue {
    std::time::SystemTime::now()
        .duration_since(std::time::SystemTime::UNIX_EPOCH)
        .map_or(RuntimeValue::Nil, |dur| {
            RuntimeValue::Number(dur.as_millis() as f64)
        })
}

fn convert_to_string(args: &[RuntimeValue]) -> RuntimeValue {
    match args {
        [val, ..] => RuntimeValue::String(val.to_string()),
        _ => RuntimeValue::Nil,
    }
}
fn convert_to_number(args: &[RuntimeValue]) -> RuntimeValue {
    match args {
        [RuntimeValue::String(s), ..] => s
            .parse()
            .map_or(RuntimeValue::Nil, |n| RuntimeValue::Number(n)),
        _ => RuntimeValue::Nil,
    }
}
