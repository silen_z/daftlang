export const fibonacci = `fun fib(n) {
  if(n < 0) return 0;
  if(n == 0) return 0;
  if(n == 1) return 1;

  return fib(n - 2) + fib(n - 1);
}

let i = 0;
while (i <= 20) {
  print fib(i);
  i = i + 1;
}`;

export const closure = `fun makeCounter(start) {
    fun counter() {
        let next = start;
        start = start + 1;
        return next;
    }
    return counter;
}

let count = makeCounter(1);

print count();
print count();
print count();
print count();
print count();`;