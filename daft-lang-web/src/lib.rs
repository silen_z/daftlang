use daft_lang::interpreter::{Interpreter, RuntimeError};
use daft_lang::parser::{Parse, ParseError};
use daft_lang::scanner::Tokenize;
use std::io::Write;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    pub type ElementConsole;

    #[wasm_bindgen(method)]
    fn print(this: &ElementConsole, text: &JsValue);
}

#[wasm_bindgen]
pub fn run(code: &str, output: ElementConsole) -> Result<(), JsValue> {
    let mut interpreter = Interpreter::new(output);

    for statement in code.chars().tokens().parse(){
        let statement = &statement.map_err(|e| WebError::ParseError(e))?;
        interpreter
            .interpret(&statement)
            .map_err(|e| WebError::RuntimeError(e))?;
    }

    Ok(())
}

impl Write for ElementConsole {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let text = &JsValue::from_str(std::str::from_utf8(buf).unwrap());
        ElementConsole::print(self, text);
        Ok(buf.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

impl From<WebError> for JsValue {
    fn from(e: WebError) -> Self {
        match e {
            WebError::RuntimeError(e) => JsValue::from_str(&format!("{:?}", e)),
            WebError::ParseError(e) => JsValue::from_str(&format!("{:?}", e)),
        }
    }
}
enum WebError {
    ParseError(ParseError),
    RuntimeError(RuntimeError),
}