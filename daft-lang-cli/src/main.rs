use daft_lang::interpreter::{Interpreter, RuntimeError};
use daft_lang::parser::{Parse, ParseError};
use daft_lang::scanner::Tokenize;
use std::env::args;
use std::fs::read_to_string;

fn main() -> Result<(), CliError> {
    let filepath = args().nth(1).ok_or(CliError::MissingFilepath)?;
    let source = read_to_string(filepath).map_err(|_| CliError::CantReadSource)?;

    let stdout = std::io::stdout();

    let mut interpreter = Interpreter::new(stdout);

    for statement in source.chars().tokens().parse() {
        interpreter.interpret(&statement?)?;
    }

    Ok(())
}

#[derive(Debug)]
pub enum CliError {
    MissingFilepath,
    CantReadSource,
    ParseError(ParseError),
    RuntimeError(RuntimeError),
}

impl From<ParseError> for CliError {
    fn from(error: ParseError) -> Self {
        CliError::ParseError(error)
    }
}

impl From<RuntimeError> for CliError {
    fn from(error: RuntimeError) -> Self {
        CliError::RuntimeError(error)
    }
}
